import tango
from tango.server import Device as Device_


class CoreDevice:
    
    def __init__(self):
        self.listener = None


class Device(Device_):

    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)
        self.core_device = CoreDevice()
    
    def delete_device(self):
        print("Device.delete_device(): entering...")
    
    def __del__(self):
        print("Device.__del__(): entering...")
