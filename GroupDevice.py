import weakref
import tango
import gc

from tango.server import Device, command


class CoreGroupDevice():

    def __init__(self, tango_device):
        self.listener = None


class GroupDevice(Device):

    def init_device(self):
        super().init_device()        
        self.set_state(tango.DevState.ON)
        self.core_device = CoreGroupDevice(self)
        sub_device = tango.Util.instance().get_device_by_name("test/device/1")
        sub_device.core_device.listener = weakref.ref(self.core_device)
    
    def delete_device(self):
        print("GroupDevice.delete_device(): entering...")
        # uncomment the following line in order to
        # avoid the problem with removing the `Device` class device.
        # self.core_device = None
    
    def __del__(self):
        print("GroupDevice.__del__(): entering...")
