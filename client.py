import sys
import tango

class Client:
    def __init__(self):
        self.pool = tango.DeviceProxy("test/pooldevice/1")

    def simple(self):
        self.pool.DeleteDevice(["Device", "test/device/1"])

    def complete(self):
        self.pool.DeleteDevice(["GroupDevice", "test/groupdevice/1"])
        self.simple()
    
    def recreate(self):
        while True:
            self.pool.CreateDevice(["Device", "test/device/1"])
            self.simple()


if __name__ == "__main__":
    mode = None
    if len(sys.argv) == 2:
        mode = sys.argv[1]
    if mode not in ("simple", "complete", "recreate"):
        raise Exception("please specify mode: 'simple', 'complete' or 'recreate")
    client = Client()
    test = getattr(client, mode)
    test()
