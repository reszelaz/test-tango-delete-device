import tango
from tango.server import Device, command


class PoolDevice(Device):

    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)

    @command(dtype_in=[str])
    def CreateDevice(self, argin):
        util = tango.Util.instance()
        klass, full_name = argin
        util.create_device(klass, full_name, alias=None, cb=None)
 
    @command(dtype_in=[str])
    def DeleteDevice(self, argin):
        util = tango.Util.instance()
        klass, full_name = argin
        device = util.get_device_by_name(full_name)
        
        if (device.core_device.listener is not None
                and device.core_device.listener() is not None):
            raise Exception(
                "can not delete device {} because it has a listener".format(full_name)
            )
        del device
        util.delete_device(klass, full_name)
      