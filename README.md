# Demonstrate problem with deleting device in a Tango DS at runtime

## Simple case

### Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`
   - 1 device of `Device` class, with the following name: `test/device/1`

    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    tango_admin --add-server DeviceServer/test Device test/device/1    
    ```
2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start client in simple mode: `python3 client.py simple`

### Result

On the device server console we see print from the `Device.delete_device()` but
no print from the `Device.__del__()`. Looks like the object is not destroyed completelly.

## Complete case

### Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`
   - 1 device of `Device` class, with the following name: `test/device/1`
   - 1 device of `GroupDevice` class, with the following name: `test/groupdevice/1`

    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    tango_admin --add-server DeviceServer/test Device test/device/1
    tango_admin --add-server DeviceServer/test GroupDevice test/groupdevice/1
    ```
2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start client in complete mode: `python3 client.py complete`

### Result

On the device server console we see print from the `GroupDevice.delete_device()` but
no print from the `GroupDevice.__del__()`. Looks like the object is not destroyed completelly.

As the consequence, the client fails to delete the `test/device/1` device cause its internal core device object still has a listener - the group core device object which could not be deleted cause the Tango device seems to not be destroyed.

## Recreate case (memory leak)

### Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`
   
    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    ```
2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start client in recreate mode: `python3 client.py recreate`

### Result

By observing the memory consumption of the DeviceServer we can see an increase of 5 MB per 1 min.

Hint: you can use [taurus_pyqtgraph](https://gitlab.com/taurus-org/taurus_pyqtgraph) 
to track the memory consumption of a process:

```
taurus trend "eval:@psutil.*/Process(<DeviceServer PID>).memory_info().rss" 
```