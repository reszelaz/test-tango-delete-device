from tango.server import run

from PoolDevice import PoolDevice
from Device import Device
from GroupDevice import GroupDevice

run([PoolDevice, Device, GroupDevice])